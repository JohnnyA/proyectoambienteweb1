/*
SELECT *
FROM isw613_questionnaires.questions;

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Doy instrucciones claras para los proyectos?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, 'Cuando los planes no se cumplen, ¿me enojo?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Apoyo a la gente cuando las cosas salen mal? ');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Les doy oportunidades de crecimiento?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Doy retroalimentación a tiempo?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Intento ser honesto/a y transparente cuando hablo con mis compañeros?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Descubro los intereses de mis compañeros e intento orientarlos hacia una meta en común?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Me considero una persona minuciosa en mi labor de enseñar a mis compañeros y de formar a otras personas?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Conozco bien a mis colaboradores y tengo un vínculo muy fuerte con ellos?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(1, '¿Intento ofrecer todo mi apoyo cuando un compañero pasa por un momento de dificultad personal?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Doy retroalimentación a tiempo?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Gestionas positivamente tus emociones?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Manifiestas tus opiniones de forma asertiva?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Confías en ti mismo y te gusta asumir retos?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Tomas la iniciativa y pasas a la acción?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Eres amoldable a las circunstancias y desafías lo tradicional por tu propias convicciones?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Eres amoldable a las circunstancias y desafías lo tradicional por tu propias convicciones?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Eres inconformista y te esfuerzas siempre por mejorar todo lo que te rodea?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Eres un líder y aportas siempre algo positivo a las personas que te rodean?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Tienes las agallas de asumir en cada momento lo que quieres hacer y cómo lo vas a hacer?');

INSERT INTO isw613_questionnaires.questions
    (questionnaire_id, question_text)
VALUES(2, '¿Asumes la responsabilidad de hacer que las cosas sucedan?');
*/
