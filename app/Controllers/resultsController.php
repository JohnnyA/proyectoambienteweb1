<?php namespace MyApp\Controllers {

use MyApp\Models\results;
use MyApp\Utils\Message;

class resultsController
{
    private $config = null;
    private $resultsModel = null;
    private $message = null;

    public function __construct($config)
    {
        $this->config = $config;
        $this->message = new Message();
    }

    public function index()
    {
        $this->resultsModel = new results($this->config);
        $message = $this->message;
        $collection = $this->resultsModel->getAllResults();
        view("results/index.php", compact("collection", "message"));
    }

    public function register()
    {
        view("results/register.php");
    }

    public function create()
    {
        view("results/create.php");
    }


    public function store()
    {
        $message = new Message();

        $questionnaire_id  = $_POST["questionnaire_id"];
        $min_value         = $_POST["min_value"];
        $max_value         = $_POST["max_value"];
        $feedback          = $_POST["feedback"];

        $resultsModel = new results($this->config);
        $result = $resultsModel->resultsExists($questionnaire_id);

        // Verificar que todos los inputs estén llenos
        if ((ltrim($feedback) == "") || (ltrim($min_value) == "") )
        {
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("results/create.php", compact("message", "questionnaire_id", "min_value", "max_value","feedback"));
            exit;
        }
        $resultsModel->insert($questionnaire_id, $min_value, $max_value, $feedback);
        $this->message->setSuccessMessage(null, "El resultado se agrego correctamente", null, true);
        $this->index();
    }

    public function edit($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->resultsModel = new results($this->config);
        $results = $this->resultsModel->getresults($id);
        view("results/edit.php", compact("results"));
    }
    
    public function show($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->resultsModel = new results($this->config);
        $results = $this->resultsModel->getResults($id);
        view("results/view.php", compact("results"));
    }
    
    public function update($login)
    {
        $message = new Message();
        $id               = $_POST["id"];
        $questionnaire_id  = $_POST["questionnaire_id"];
        $min_value         = $_POST["min_value"];
        $max_value         = $_POST["max_value"];
        $feedback          = $_POST["feedback"];
        
        $this->resultsModel = new results($this->config);
        $results = $this->resultsModel->getResults($id);
        
        if ($id != null)
        {

            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->resultsModel->isSuperResults($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para actualizar el registro", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }
            

            // Si pasó todas la verificaciones hago el insert
            $this->resultsModel->update($id,$questionnaire_id, $min_value, $max_value, $feedback);
            $this->message->setSuccessMessage(null, "Se actualizo el resultado correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál resultado actualizar", null, true);
            $this->index();
        }
        // Si pasó todas la verificaciones hago el delete
        
    }

    public function destroy($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->resultsModel = new results($this->config);

        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->resultsModel->isSuperResults($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para eliminar el resultado", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }

            // Si pasó todas la verificaciones hago el delete
            $this->resultsModel->delete($id);
            $this->message->setSuccessMessage(null, "Se eliminó el resultado correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál resultado borrar", null, true);
            $this->index();
        }
    }
}
}