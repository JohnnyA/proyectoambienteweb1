<?php namespace MyApp\Controllers {

use MyApp\Models\questions;
use MyApp\Utils\Message;

class questionsController
{
    private $config = null;
    private $questionsModel = null;
    private $message = null;

    public function __construct($config)
    {
        $this->config = $config;
        $this->message = new Message();
    }

    public function index()
    {
        $this->questionsModel = new questions($this->config);
        $message = $this->message;
        $collection = $this->questionsModel->getAllQuestions();
        view("questions/index.php", compact("collection", "message"));
    }

    public function create()
    {
        view("questions/create.php");
    }

    public function store()
    {

        $message = new Message();
        $questionnaire_id        = $_POST["questionnaire_id"];
        $question_text   = $_POST["question_text"];

        $questionsModel = new questions($this->config);
        $result = $questionsModel->questionsExists($question_text);

        // Verificar que todos los inputs estén llenos
        if (( ltrim($question_text) == ""))
        {
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("questions/create.php", compact("message", "questionnaire_id", "question_text"));
            exit;
        }

        // Verifico si el usuario ya existe
        if ($result["exists"] == 1)
        {
            $message->setWarningMessage(null, "La pregunta ya existe", null, true);
            view("questions/create.php", compact("message", "questionnaire_id", "question_text"));
            exit;
        }

        // Si pasó todas la verificaciones hago el insert
        $questionsModel->insert($questionnaire_id,$question_text);
        
        //header("Location: /questionnaires/index.php");
        $this->message->setSuccessMessage(null, "La pregunta se agregó correctamente", null, true);
        $this->index();
    }

    public function show($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionsModel = new questions($this->config);
        $questions = $this->questionsModel->getQuestions($id);
        view("questions/view.php", compact("questions"));
    } 

    public function edit($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionsModel = new questions($this->config);
        $questions = $this->questionsModel->getQuestions($id);
        view("questions/edit.php", compact("questions"));
    }
    
    public function update($login)
    {
        
        $message = new Message();
        $id                  = $_POST["id"];
        $questionnaire_id         = $_POST["questionnaire_id"];
        $question_text    = $_POST["question_text"];
        $this->questionsModel = new questions($this->config);
        $questions = $this->questionsModel->getQuestions($id);
        
        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->questionsModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para actualizar la pregunta", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }
            // Si pasó todas la verificaciones hago el insert
            $this->questionsModel->update($id,$questionnaire_id, $question_text);
            $this->message->setSuccessMessage(null, "Se actualizo la pregunta correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál pregunta actualizar", null, true);
            $this->index();
        }
        // Si pasó todas la verificaciones hago el delete
        
    }

    public function destroy($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionsModel = new questions($this->config);

        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->questionsModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para eliminar la pregunta", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }

            // Si pasó todas la verificaciones hago el delete
            $this->questionsModel->delete($id);
            $this->message->setSuccessMessage(null, "Se eliminó la pregunta correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál pregunta borrar", null, true);
            $this->index();
        }
    }
}
}