<?php namespace MyApp\Controllers {

use MyApp\Models\answers;
use MyApp\Utils\Message;

class answersController
{
    private $config = null;
    private $answersModel = null;
    private $message = null;

    public function __construct($config)
    {
        $this->config = $config;
        $this->message = new Message();
    }

    public function index()
    {
        $this->answersModel = new answers($this->config);
        $message = $this->message;
        $collection = $this->answersModel->getAllAnswers();
        view("answers/index.php", compact("collection", "message"));
    }

    public function create()
    {
        view("answers/create.php");
    }

    public function store()
    {
        $message = new Message();
        $question_id        = $_POST["question_id"];
        $number        = $_POST["number"];
        $answer_text   = $_POST["answer_text"];
        $answer_points   = $_POST["answer_points"];

        $answersModel = new answers($this->config);
        $answer = $answersModel->answersExists($answer_text);

        // Verificar que todos los inputs estén llenos
        if (( ltrim($answer_text) == ""))
        {
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("answers/create.php", compact("message", "question_id", "number","answer_text","answer_points"));
            exit;
        }

        // Si pasó todas la verificaciones hago el insert
        $answersModel->insert($question_id,$number,$answer_text,$answer_points);
        //header("Location: /anwers/index.php");
        $this->message->setSuccessMessage(null, "La respuesta se agregó correctamente", null, true);
        $this->index();
    }

    public function show($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->answersModel = new answers($this->config);
        $answers = $this->answersModel->getAnswers($id);
        view("answers/view.php", compact("answers"));
    }

    public function edit($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->answersModel = new answers($this->config);
        $answers = $this->answersModel->getAnswers($id);
        view("answers/edit.php", compact("answers"));
    }
    
    public function update($login)
    {
        
        $message = new Message();
        $id             =   $_POST["id"];
        $question_id    =   $_POST["question_id"];
        $number         =   $_POST["number"];
        $answer_text    =   $_POST["answer_text"];
        $answer_points  =   $_POST["answer_points"];
        
        $this->answersModel = new answers($this->config);
        $answers = $this->answersModel->getAnswers($id);
        
        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar la respuesta
            if (!$this->answersModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para actualizar la respuesta", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }
            

            // Si pasó todas la verificaciones hago el update
            $this->answersModel->update($id,$question_id,$number,$answer_text, $answer_points);
            $this->message->setSuccessMessage(null, "Se actualizo la respuesta correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál respuesta actualizar", null, true);
            $this->index();
        }
        
    }

    public function destroy($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->answersModel = new answers($this->config);

        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->answersModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para eliminar la respuesta", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }

            // Si pasó todas la verificaciones hago el delete
            $this->answersModel->delete($id);
            $this->message->setSuccessMessage(null, "Se eliminó la respuesta correctamente", null, true);
            var_dump($id);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál respuesta borrar", null, true);
            $this->index();
        }
    }
}
}