<?php namespace MyApp\Controllers {

use MyApp\Models\questionnaires;
use MyApp\Utils\Message;

class questionnairesController
{
    private $config = null;
    private $questionnairesModel = null;
    private $message = null;

    public function __construct($config)
    {
        $this->config = $config;
        $this->message = new Message();
    }

    public function index()
    {
        $this->questionnairesModel = new questionnaires($this->config);
        $message = $this->message;
        $collection = $this->questionnairesModel->getAllQuestionnaires();
        
        //var_dump($colletion);
        view("questionnaires/index.php", compact("collection", "message"));
    }

    public function create()
    {
        view("questionnaires/create.php");
    }

    public function store()
    {
        $message = new Message();

        //var_dump($_POST);
        
        $description        = $_POST["description"];
        $long_description   = $_POST["long_description"];
        $questionnairesModel = new questionnaires($this->config);
        $result = $questionnairesModel->questionnairesExists($description);

        // Verificar que todos los inputs estén llenos
        if ((ltrim($description) == "") || (ltrim($long_description) == ""))
        {
            $message->setWarningMessage(null, "Todos los campos son requeridos", null, true);
            view("questionnaires/create.php", compact("message", "description", "long_description"));
            exit;
        }

        // Verifico si el usuario ya existe
        if ($result["exists"] == 1)
        {
            $message->setWarningMessage(null, "cuestionario ya existe", null, true);
            view("users/create.php", compact("message", "description", "long_description"));
            exit;
        }

        // Si pasó todas la verificaciones hago el insert
        $questionnairesModel->insert($description,$long_description);
        
        //header("Location: /questionnaires/index.php");
        $this->message->setSuccessMessage(null, "El cuestionario se agregó correctamente", null, true);
        $this->index();
    }

    public function show($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionnairesModel = new questionnaires($this->config);
        $questionnaires = $this->questionnairesModel->getQuestionnaires($id);
        view("questionnaires/view.php", compact("questionnaires"));
    }

    public function edit($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionnairesModel = new questionnaires($this->config);
        $questionnaires = $this->questionnairesModel->getQuestionnaires($id);
        view("questionnaires/edit.php", compact("questionnaires"));
    }
    
    public function update($login)
    {
        
        $message = new Message();
        $id                  = $_POST["id"];
        $description         = $_POST["description"];
        $long_description    = $_POST["long_description"];
        
        $this->questionnairesModel = new questionnaires($this->config);
        $questionnaires = $this->questionnairesModel->getQuestionnaires($id);
        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->questionnairesModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para actualizar el cuestionario", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }

            // Si pasó todas la verificaciones hago el insert
            $this->questionnairesModel->update($id,$description, $long_description);
            $this->message->setSuccessMessage(null, "Se actualizo el cuestionario correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál cuestionario actualizar", null, true);
            $this->index();
        }
        // Si pasó todas la verificaciones hago el delete
        
    }

    public function destroy($login)
    {
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $this->questionnairesModel = new questionnaires($this->config);

        if ($id != null)
        {
            // Verificar si el usuario tiene permisos para borrar el registro
            if (!$this->questionnairesModel->isSuperuser($login['id'])){
                $this->message->setDangerMessage(null, "Usted no tiene permisos para eliminar el cuestionario", null, true);
                $message = $this->message;
                view("auth/forbidden.php", compact("message"));
                exit;
            }

            // Si pasó todas la verificaciones hago el delete
            $this->questionnairesModel->delete($id);
            $this->message->setSuccessMessage(null, "Se eliminó el cuestionario correctamente", null, true);
            $this->index();
        }
        else
        {
            $this->message->setWarningMessage(null, "No sé cuál cuestionario borrar", null, true);
            $this->index();
        }
    }
}
}