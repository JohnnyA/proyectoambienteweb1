<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
<?php include("../db.php"); ?>
<?php
use \EasilyPHP\Database\DBMySQL;
use MyApp\Models\results;
?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Crear resultados</h1>
          <!-- Inicia el formulario de create -->
          <form action="/results/index.php?action=store" method="post">
          <div class="form-group">
              <label for="questionnaire_id">Cuestionarios:</label>
              <select  class="form-control" id="questionnaire_id" name="questionnaire_id">
                  <option disabled value="" selected="selected">Cuestionarios</option>
                  <?php
                      $query = "SELECT * FROM questionnaires";
                      $questionnaires = mysqli_query($conn, $query);
                      while($row = mysqli_fetch_assoc($questionnaires)) {
                        ?>
                        <option value="<?= $row['id']?>"><?= $row['description']?></option>;
                        <?php
                        extract($row);
                    }                   
                      ?>
           </select>
            </div>
            <div class="form-group">
              <label for="min_value">Min:</label>
              <input
                type="number" class="form-control"
                id="min_value" name="min_value"
                aria-describedby="Introduzca el valor minimo"
                placeholder="" value="<?= isset($min_value) ? $min_value : ""; ?>">
            </div>
            <div class="form-group">
              <label for="max_value">Max:</label>
              <input
                type="number" class="form-control"
                id="max_value" name="max_value"
                aria-describedby="Introduzca el valor maximo"
                placeholder="" value="<?= isset($max_value) ? $max_value : ""; ?>">
            </div>
            <div class="form-group">
              <label for="max_value">feedback:</label>
              <input
                type="text" class="form-control"
                id="feedback" name="feedback"
                aria-describedby="Introduzca el feedback del cuestinario"
                placeholder="" value="<?= isset($feedback) ? $feedback : ""; ?>">
            </div>
            <button type="submit" class="btn" style="background-color:#5D89A3; display:inline;">Guardar</button>
            <a class="btn btn-secondary" href="/results/index.php">Cancelar</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>