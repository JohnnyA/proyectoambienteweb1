<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
    <div class="row">
        <div class="col-md-8">
            <h1>Resultados</h1>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">#Cuestionario</th>
                <th scope="col">Min</th>
                <th scope="col">Max</th>
                <th scope="col">feedback</th>
                <th scope="col">Eliminar</th>
                <th scope="col">Ver</th>
                <th scope="col">Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($collection as $record) : ?>
                <tr>
                    <th scope="row"><?= $record["id"] ?></th>
                    <td><?= $record["questionnaire_id"] ?></td>
                    <td><?= $record["min_value"] ?></td>
                    <td><?= $record["max_value"] ?></td>
                    <td><?= $record["feedback"] ?></td>
                    <td class="text-center">
                        <a href="/results/index.php?action=destroy&id=<?= $record["id"] ?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/results/index.php?action=show&id=<?= $record["id"] ?>">
                            <i class="fas fa-eye"></i> 
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/results/index.php?action=edit&id=<?= $record["id"] ?>">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
             </tbody>
            </table>
        </div>
    </div>
  <?php include VIEWS.'/partials/footer.php' ?>