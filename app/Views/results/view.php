<?php
  $id=isset($id) ? $id : $results['id'];
  $questionnaire_id = isset($questionnaire_id) ? $questionnaire_id : $results['questionnaire_id'];
  $min_value = isset($min_value) ? $min_value : $results['min_value'];
  $max_value = isset($max_value) ? $max_value : $results['max_value'];
  $feedback = isset($feedback) ? $feedback : $results['feedback'];
?>
<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Ver resultado</h1>
          <!-- Inicia el formulario de create -->
          
          <form action="/results/index.php?action=show&id=<?=$results['id']?>" method="post">
            <div class="form-group"hidden>
              <label for="id">Id</label>
              <input disabled
                type="text" class="form-control"
                id="id" name="id"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($id) ? $id : ""; ?>">
            </div>
            <div class="form-group">
              <label for="questionnaire_id">Cuestionario</label>
              <input disabled
                type="text" class="form-control"
                id="questionnaire_id" name="questionnaire_id"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($questionnaire_id) ? $questionnaire_id : ""; ?>">
            </div>
            <div class="form-group">
              <label for="min_value">Valor minimo de puntos</label>
              <input disabled
                type="text" class="form-control"
                id="min_value" name="min_value"
                aria-describedby="Min value"
                placeholder="" value="<?= isset($min_value) ? $min_value : ""; ?>">
            </div>
            <div class="form-group">
              <label for="min_value">Valor maximo de puntos</label>
              <input disabled
                type="text" class="form-control"
                id="max_value" name="max_value"
                aria-describedby="Max value"
                placeholder="" value="<?= isset($max_value) ? $max_value : ""; ?>">
            </div>
            <div class="form-group">
              <label for="feedback">Realimentacion</label>
              <input disabled
                type="text" class="form-control"
                id="feedback" name="feedback"
                aria-describedby="Max value"
                placeholder="" value="<?= isset($feedback) ? $feedback : ""; ?>">
            </div>
            <a class="btn btn-secondary" href="/results/index.php">Regresar a la lista</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>