<?php
  $id=isset($id) ? $id : $answers['id'];
  $question_id=isset($question_id) ? $question_id : $answers['question_id'];
  $answer_text = isset($answer_text) ? $answer_text : $answers['answer_text'];
  $answer_points = isset($answer_points) ? $answer_points : $answers['answer_points'];
?>
<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Ver respuestas</h1>
          <!-- Inicia el formulario de create -->
          
          <form action="/answers/index.php?action=show&id=<?php $answers['id']?>" method="post">
            <div class="form-group" hidden>
              <label for="id">Is</label>
              <input disabled
                type="text" class="form-control"
                id="id" name="id"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($id) ? $id : ""; ?>">
            </div>
            <div class="form-group">
              <label for="question_id">Numero de la Pregunta</label>
              <input disabled
                type="text" class="form-control"
                id="question_id" name="question_id"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($question_id) ? $question_id : ""; ?>">
            </div>
            <div class="form-group">
              <label for="answer_text">Respuesta</label>
              <input disabled
                type="text" class="form-control"
                id="answer_text" name="answer_text"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($answer_text) ? $answer_text : ""; ?>">
            </div>
            <div class="form-group">
              <label for="answer_points">Puntos de la pregunta</label>
              <input disabled
                type="text" class="form-control"
                id="answer_points" name="answer_points"
                aria-describedby="answer_points"
                placeholder="" value="<?= isset($answer_points) ? $answer_points : ""; ?>">
            </div>
            <a class="btn btn-secondary" href="/answers/index.php">Regresar a la lista</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>