<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
    <div class="row">
        <div class="col-md-8">
            <h1>Respuestas</h1>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">#Pregunta</th>
                <th scope="col">Respuestas</th>
                <th scope="col">Puntos</th>
                <th scope="col">Eliminar</th>
                <th scope="col">Ver</th>
                <th scope="col">Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($collection as $record) : ?>
                <tr>
                    <th scope="row"><?= $record["id"] ?></th>
                    <td><?= $record["question_id"] ?></td>
                    <td><?= $record["answer_text"] ?></td>
                    <td><?= $record["answer_points"] ?></td>
                    <td class="text-center">
                        <a href="/answers/index.php?action=destroy&id=<?= $record["id"] ?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/answers/index.php?action=show&id=<?= $record["id"] ?>">
                            <i class="fas fa-eye"></i> 
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/answers/index.php?action=edit&id=<?= $record["id"] ?>">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
             </tbody>
            </table>
        </div>
    </div>
  <?php include VIEWS.'/partials/footer.php' ?>