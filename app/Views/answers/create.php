<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
<?php include("../db.php"); ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Crear respuestas</h1>
          <!-- Inicia el formulario de create -->
          <form action="/answers/index.php?action=store" method="post">
          <div class="form-group">
              <label for="questionnaire_id">Cuestionarios:</label>
              <select  class="form-control" id="questionnaire_id" name="questionnaire_id">
                  <option disabled value="" selected="selected">Cuestionarios</option>
                  <?php
                      $query = "SELECT * FROM questionnaires";
                      $questionnaires = mysqli_query($conn, $query);
                      while($row = mysqli_fetch_assoc($questionnaires)) {
                        ?>
                        <option value="<?= $row['id']?>"><?= $row['description']?></option>;
                        <?php
                        extract($row);
                        }                   
                    ?>
                </select>
            </div>
          <div class="form-group">
          <label for="question_id">Preguntas:</label>
          <select  class="form-control" id="question_id" name="question_id">
              <option value="" selected="selected">Preguntas</option>
              <?php
                  $query = "SELECT * FROM questions";
                  $questions = mysqli_query($conn, $query);
                  while($row = mysqli_fetch_assoc($questions)) {
                    ?>
                     <option value="<?= $row['id']?>"><?= $row['question_text']?></option>;
                     <?php
                     extract($row);
                }                   
                  ?>
           </select>
            </div>
            <div class="form-group">
              <label for="answer_text">Numero de pregunta:</label>
              <input
                type="number" class="form-control"
                id="number" name="number"
                aria-describedby="Introduzca el numero de la pregunta"
                placeholder="" value="<?= isset($number) ? $number : ""; ?>">
            </div>
            <div class="form-group">
              <label for="answer_text">Respuesta:</label>
              <input
                type="text" class="form-control"
                id="answer_text" name="answer_text"
                aria-describedby="Introduzca la respuesta"
                placeholder="" value="<?= isset($answer_text) ? $answer_text : ""; ?>">
            </div>
            <div class="form-group">
              <label for="answer_text">Puntos:</label>
              <input
                type="number" class="form-control"
                id="answer_points" name="answer_points"
                aria-describedby="Introduzca los puntos de la respuesta"
                placeholder="" value="<?= isset($answer_points) ? $answer_points : ""; ?>">
            </div>
            <button type="submit" class="btn"style="background-color:#5D89A3; display:inline;">Guardar</button>
            <a class="btn btn-secondary" href="/answers/index.php">Cancelar</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>