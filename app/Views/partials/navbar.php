<nav class="navbar navbar-expand-lg navbar-light"style="background-color: #6BA0C0;">
  <a class="navbar-brand" href="/">ISW613</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/index.php">Inicio <span class="sr-only">(current)</span></a>
      </li>
      <!--li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li-->
      <?php if (isset($_SESSION['login']) && !is_null($_SESSION['login'])) : ?>
        <?php if ($_SESSION['login']['duty'] == 'S') : ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Usuarios
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
              <a class="dropdown-item" href="/users/index.php">Ver todos los usuarios</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/users/index.php?action=create">Agregar usuario</a>
            </div>
          </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Catalogo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
          <a class="dropdown-item" href="/questionnaires/index.php">Ver todos los cuestionarios</a>
          <a class="dropdown-item" href="/questionnaires/index.php?action=create">Agregar cuestionario</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/questions/index.php">Ver todas las preguntas</a>
          <a class="dropdown-item" href="/questions/index.php?action=create">Agregar preguntas</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/answers/index.php">Ver todas las respuestas</a>
          <a class="dropdown-item" href="/answers/index.php?action=create">Agregar respuestas</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/results/index.php">Ver todos los resultados</a>
          <a class="dropdown-item" href="/results/index.php?action=create">Agregar resultados</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cuestionarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
          <a class="dropdown-item" href="/cuestionarios/formularioLiderazgo.php">Liderazgo</a>
          <a class="dropdown-item" href="/cuestionarios/formularioProactividad.php">Proactividad</a>
          <div class="dropdown-divider"></div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Reportes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
          <a class="dropdown-item" href="/grafico/examples/reportes/reporte1.php">Reporte por empleado</a>
          <a class="dropdown-item" href="/grafico/examples/reportes/vista_menu.php">Reporte por cuestionario</a>
          <a class="dropdown-item" href="/grafico/examples/reportes/reporte3.php">Grafico top 10</a>
          <div class="dropdown-divider"></div>
      </li>
        <?php else: ?>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Catalogo
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
          <a class="dropdown-item" href="/questionnaires/index.php">Ver todos los cuestionarios</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/questions/index.php">Ver todas las preguntas</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cuestionarios
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
          <a class="dropdown-item" href="/cuestionarios/formularioLiderazgo.php">Liderazgo</a>
          <a class="dropdown-item" href="/cuestionarios/formularioProactividad.php">Proactividad</a>
          <div class="dropdown-divider"></div>
      </li>
        <?php endif;?>
    </ul>
    <?php else : ?>
    </ul>
    <?php endif; ?>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <?php if (isset($_SESSION['login']) && !is_null($_SESSION['login'])) : ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $_SESSION['login']['username'] ?>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"style="background-color: #E1F3FE;">
                <a class="dropdown-item" href="/authenticate/index.php?action=logout.php">Cerrar sesión</a>
              </div>
            </li>
          <?php else : ?>
            <li class="nav-item">
              <a class="nav-link" href="/authenticate/index.php?action=login">Iniciar sesión</a>
            </li>
          <?php endif; ?>
      </li>
    </ul>

  </div>
</nav>
<br>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>