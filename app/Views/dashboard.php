<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
  <div class="container">
      <br>
      <div class="jumbotron"style="background-color: #6BA0C0;">
          <h3 align="center">AvanSoftware</h3>
          <?php if (is_null($login)) : ?>
              <p style="display:inline;"><a align="center" class="btn btn-lg" style="background-color:#5D89A3;display:inline;"
              href="/authenticate/index.php?action=login" role="button">Iniciar sesión</a></p>
              <p style="display:inline;"><a  class="btn btn-lg" style="background-color:#5D89A3; display:inline;"
              href="/users/index.php?action=register" role="button">Registrarse</a></p>
          <?php else : ?>
            <br>
            <h4 align="center">Bienvenido <?= $login['fullname'] ?></h4>
          <?php endif; ?>
      </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
