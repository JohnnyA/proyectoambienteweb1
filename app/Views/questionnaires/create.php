<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Crear cuestionario</h1>
          <!-- Inicia el formulario de create -->
          <form action="/questionnaires/index.php?action=store" method="post">
            <div class="form-group">
              <label for="description">Description:</label>
              <input
                type="text" class="form-control"
                id="description" name="description"
                aria-describedby="Introduzca la descripcion del cuestionario"
                placeholder="" value="<?= isset($description) ? $description : ""; ?>">
            </div>
            <div class="form-group">
              <label for="long_description">Description detallada:</label>
              <input
                type="text" class="form-control"
                id="long_description" name="long_description"
                aria-describedby="Introduzca la descripcion detallada"
                placeholder="" value="<?= isset($long_description) ? $long_description : ""; ?>">
            </div>
            <button type="submit" class="btn"style="background-color:#5D89A3; display:inline;">Guardar</button>
            <a class="btn btn-secondary" href="/questionnaires/index.php">Cancelar</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>