<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
    <div class="row">
        <div class="col-md-8">
            <h1>Cuestionarios</h1>
            <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Descripcion detallada</th>
                <th scope="col">Eliminar</th>
                <th scope="col">Ver</th>
                <th scope="col">Editar</th>
                <th scope="col">Preguntas</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($collection as $record) : ?>
                <tr>
                    <th scope="row"><?= $record["id"] ?></th>
                    <td><?= $record["description"] ?></td>
                    <td><?= $record["long_description"] ?></td>
                    <td class="text-center">
                        <a href="/questionnaires/index.php?action=destroy&id=<?= $record["id"] ?>">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/questionnaires/index.php?action=show&id=<?= $record["id"] ?>">
                            <i class="fas fa-eye"></i> 
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/questionnaires/index.php?action=edit&id=<?= $record["id"] ?>">
                            <i class="fas fa-edit"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a href="/questions/index.php?>">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach; ?>
             </tbody>
            </table>
        </div>
    </div>
  <?php include VIEWS.'/partials/footer.php' ?>