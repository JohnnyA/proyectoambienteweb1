<?php
  $id=isset($id) ? $id : $questions['id'];
  $questionnaires_id = isset($questionnaires_id) ? $questionnaires_id : $questions['questionnaire_id'];
  $question_text = isset($question_text) ? $question_text : $questions['question_text'];

?>
<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
<?php include("../db.php"); ?>
<?php
use \EasilyPHP\Database\DBMySQL;
use MyApp\Models\questions;
?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h1>Ver pregunta</h1>
          <!-- Inicia el formulario de create -->
            <form action="/questions/index.php?action=show&id=<?php $questions['id'] ?>>" method="post">
                <div class="form-group"hidden>
                    <label for="id">Id:</label>
                    <input
                        type="text" class="form-control"
                        id="id" name="id"
                        aria-describedby="Introduzca la descripcion detallada"
                        placeholder="" value="<?= isset($id) ? $id : ""; ?>">
                </div>
                <div class="form-group">
                    <label for="questionnaire_id">Id del Cuestionario:</label>
                    <input disabled
                        type="text" class="form-control"
                        id="questionnaire_id" name="questionnaire_id"
                        aria-describedby="Introduzca el id del cuestionario"
                        placeholder="" value="<?= isset($questionnaires_id) ? $questionnaires_id : ""; ?>">
                </div>
                <div class="form-group">
                  <label for="question_text">Pregunta:</label>
                  <input disabled
                    type="text" class="form-control"
                    id="question_text" name="question_text"
                    aria-describedby="Introduzca la descripcion detallada"
                    placeholder="" value="<?= isset($question_text) ? $question_text : ""; ?>">
                </div>
            <a class="btn btn-secondary" href="/questions/index.php">Cancelar</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>