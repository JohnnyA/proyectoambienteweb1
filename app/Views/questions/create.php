<?php include VIEWS.'/partials/header.php' ?>
<?php include VIEWS.'/partials/navbar.php' ?>
<?php include("../db.php"); ?>
<?php
use \EasilyPHP\Database\DBMySQL;
use MyApp\Models\questionnaires;
?>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php include VIEWS.'/partials/message.php' ?>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
          <h1>Crear preguntas</h1>
          <!-- Inicia el formulario de create -->
          <form action="/questions/index.php?action=store" method="post">
          <div class="form-group">
              <label for="questionnaire_id">Cuestionarios:</label>
              <select  class="form-control" id="questionnaire_id" name="questionnaire_id">
                  <option disabled value="" selected="selected">Cuestionarios</option>
                  <?php
                      $query = "SELECT * FROM questionnaires";
                      $questionnaires = mysqli_query($conn, $query);
                      while($row = mysqli_fetch_assoc($questionnaires)) {
                        ?>
                        <option value="<?=intval($row['id']);?>"><?= $row['description']?></option>;
                        <?php
                        extract($row);
                    }                   
                      ?>
           </select>
            </div>
            <div class="form-group">
              <label for="question_text">Pregunta:</label>
              <input
                type="text" class="form-control"
                id="question_text" name="question_text"
                aria-describedby="Introduzca la descripcion detallada"
                placeholder="" value="<?= isset($question_text) ? $question_text : ""; ?>">
            </div>
            <button type="submit" class="btn"style="background-color:#5D89A3; display:inline;">Guardar</button>
            <a class="btn btn-secondary" href="/questionnaires/index.php">Cancelar</a>
          </form>
        </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>