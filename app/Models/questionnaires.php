<?php namespace MyApp\Models {
    
    use \EasilyPHP\Database\DBMySQL;

    class questionnaires
    {
        private $db = null;

        public function __construct($config)
        {
            $this->db = new DBMySQL(
                $config['server'], $config['database'], 
                $config['user'], $config['password']);
        }

        public function getAllQuestionnaires()
        {
            $this->db->connect();
            $result = $this->db->runSql("SELECT * FROM questionnaires");
            $this->db->disconnect();
            return $this->db->getAll($result);
        }

        public function getQuestionnaires($id)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt = 
                $this->db->prepareSql("SELECT * FROM questionnaires WHERE `id` = ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("i", $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
            
            $result = $stmt->get_result();
            //var_dump($this->db->nextResultRow($result));
            $this->db->disconnect();
            return $this->db->nextResultRow($result);
        }


        public function insert($description, $log_description)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt = 
                $this->db->prepareSql("INSERT INTO questionnaires(`description`, `long_description`) 
                    VALUES (?, ?)"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("ss", $description, $log_description)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
            
            $this->db->disconnect();
        }

        public function update($id, $description, $log_description){
            $this->db->connect();
            
            if (!($stmt = 
                $this->db->prepareSql("UPDATE questionnaires set `description`=?, `long_description`=?
                    WHERE `id`=?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }
            if (!$stmt->bind_param('sss',$description, $log_description, $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();

        }

        public function delete($id)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt = 
                $this->db->prepareSql("DELETE FROM questionnaires WHERE `id` = ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("i", $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $this->db->disconnect();
        }

        public function questionnairesExists($description)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt = 
                $this->db->prepareSql("SELECT count(1) as `exists` FROM questionnaires WHERE `description` = ?"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("s", $description)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $result = $stmt->get_result();
            $this->db->disconnect();
            return $this->db->nextResultRow($result);
        }

        public function isSuperuser($id)
        {
            $this->db->connect();

            /* Prepared statement, stage 1: prepare */
            if (!($stmt = 
                $this->db->prepareSql("SELECT count(1) as `issuper` FROM users WHERE `id` = ? AND `duty` = 'S'"))) {
                echo "Prepare failed: (" .  $this->db->getError() . ") " . $this->db->getErrorMessage();
            }

            /* Prepared statement, stage 2: bind and execute */
            if (!$stmt->bind_param("i", $id)) {
                echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }

            $result = $stmt->get_result();
            $this->db->disconnect();
            return ($this->db->nextResultRow($result)['issuper'] == '1') ? true : false;
        }
    }
}