<?php include("db.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<title>MyApp </title>
	<link rel="stylesheet" type="text/css" href="./librerias/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./librerias/bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="./librerias/bootstrap/css/all.css">
	<script src="./librerias/bootstrap/js/bootstrap.bundle.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="./librerias/bootstrap/js/jquery.min.js"></script>
	<script type="text/javascript" src="./librerias/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="control.js"></script>
</head>
<body>
	<center><h1> Reporte por empleado </h1></center>
 <center> <button class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal_selector"> Consultar </button></center>
 <br>
 <br>
 <center><a class="btn btn-info btn-md" href="/index.php">Cancelar</a></center>   
    	<div id="panel_listado">
    		<!-- Panel de datos -->
    	</div>
</body>
</html>
<!-- Modal -->
<div id="myModal_selector" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #084B8A; color:white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Selector </h4>
      </div>
      <div class="modal-body">
        <p> Seleccion </p>
       <select class="form-control" id="select_usuario" onchange="select_usuario();">
        <option value=""> Seleccione </option>
		<?php
                      $query = "SELECT * FROM users";
                      $questionnaires = mysqli_query($conn, $query);
                      while($row = mysqli_fetch_assoc($questionnaires)) {
                        ?>
                        <option value="<?=intval($row['id']);?>"><?= $row['fullname']?></option>;
                        <?php
                        extract($row);
                    }                   
                      ?>
        </select>
        <div id="panel_selector"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cerrar </button>
      </div>
    </div>
  </div>
</div>