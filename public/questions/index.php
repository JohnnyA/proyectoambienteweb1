<?php
    // Esta es el "dispatcher" de las preguntas

    // En cada script hay que cargar el init.php
    include_once $_SERVER['DOCUMENT_ROOT']."/../app/init.php";

    // Se cargan las clases que se ocupan
    include_once MODELS."/questions.php";
    include_once CONTROLLERS."/questionsController.php";

    // Se hace el "use" de las clases que se utilizaran en este script
    use MyApp\Controllers\questionsController;
    $controller = new questionsController($config);

    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            
            case 'show':
                $controller->show($login);
                break;
            case 'create':
                $controller->create();
                break;
            case 'store':
                $controller->store();
                break;
            case 'edit':
                $controller->edit($login);
                break;
            case 'update':
                $controller->update($login);
                break;
            case 'destroy':
                $controller->destroy($login);
                break;
            default:
                $controller->index();
                break;
        }
    }
    else
    {
        $controller->index();
    }
